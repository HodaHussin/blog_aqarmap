<?php

namespace Blogger\BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="Blogger\BlogBundle\Repository\CategoryRepository")
 */
class Category {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * Get id
     *
     * @return int
     */
    private $blogPosts;
    

    public function __construct() {
       $this->blogPosts = new ArrayCollection();
    }

    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    
    public function getBlogPosts() {
        return $this->$blogPosts;
    }

    /**
     * Add blog
     *
     * @param \Blogger\BlogBundle\Entity\BlogPost $blog
     *
     * @return Category
     */
    public function addBlog(\Blogger\BlogBundle\Entity\BlogPost $blogPosts) {
        $this->blogPosts[] = $blogPosts;

        return $this;
    }

    /**
     * Remove blog
     *
     * @param \Blogger\BlogBundle\Entity\BlogPost $blog
     */
    public function removeBlog(\Blogger\BlogBundle\Entity\BlogPost $blogPosts) {
        $this->blog->removeElement($blogPosts);
    }

    /**
     * Get blog
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBlog() {
        return $this->blogPosts;
    }

}

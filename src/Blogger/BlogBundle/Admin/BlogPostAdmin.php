<?php

namespace Blogger\BlogBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\HttpFoundation\Request;

class BlogPostAdmin extends AbstractAdmin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper->add('title', 'text')->add('body', NULL, array('attr' => array(
                        'class' => 'tinymce', 'data-theme' => 'simple')))
                ->add('category', 'entity', array(
                    'class' => 'Blogger\BlogBundle\Entity\Category',
                    'choice_label' => 'name',
                        )
                )->add('image', 'file', array('data_class' => null))->add('tags', 'text')
        ;
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('title')
                ->add('category.name')
                ->add('draft')
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'delete' => array(),
                        'edit' => array(),
        )));
    }

    public function toString($object) {
        return $object instanceof BlogPost ? $object->getTitle() : 'Blog Post'; // shown in the breadcrumb on the create view
    }

    public function prePersist($object) {
        $this->saveFile($object);
    }

    public function preUpdate($object) {
        $this->saveFile($object);
    }

    public function saveFile($object) {

        $basepath = $this->getRequest()->getBasePath();
        $object->upload($basepath);
    }

}

<?php

namespace Blogger\BlogBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class CategoryAdmin extends AbstractAdmin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper->add('name', 'text');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper->addIdentifier('name')->add('_action', 'actions', array(
            'actions' => array(
                'show' => array(),
                'delete' => array(),
                'edit' => array())
        ));
    }

    public function toString($object) {
        return $object instanceof BlogPost ? $object->getTitle() : 'Category'; // shown in the breadcrumb on the create view
    }

}

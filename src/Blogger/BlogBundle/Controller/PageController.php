<?php

namespace Blogger\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Blogger\BlogBundle\Entity\Contact;
use Blogger\BlogBundle\Form\ContactType;
use Symfony\Component\HttpFoundation\Request;

class PageController extends Controller {

   public function indexAction()
    {
        $em = $this->getDoctrine()
                   ->getEntityManager();

         $blogs = $em->getRepository('BloggerBlogBundle:BlogPost')
                    ->getLatestBlogs();
         
         $categories = $em->getRepository('BloggerBlogBundle:Category')
                    ->getAllCategories();

        return $this->render('BloggerBlogBundle:Page:index.html.twig', array(
            'blogs' => $blogs,'categories'=>$categories
        ));
    }

    public function aboutAction() {
        return $this->render('BloggerBlogBundle:Page:about.html.twig');
    }

    public function contactAction() {


        $enquiry = new Contact();
        $form = $this->createForm(ContactType::class, $enquiry);

        $request = Request::createFromGlobals();
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $message = \Swift_Message::newInstance()
                        ->setSubject('Contact enquiry from symblog')
                        ->setFrom('enquiries@symblog.co.uk')
                        ->setTo('email@email.com')
                        ->setBody($this->renderView('BloggerBlogBundle:Page:contactEmail.txt.twig', array('enquiry' => $enquiry)));
                $this->get('mailer')->send($message);

                $this->get('session')->setFlash('blogger-notice', 'Your contact enquiry was successfully sent. Thank you!');
                return $this->redirect($this->generateUrl('BloggerBlogBundle_contact'));
            }
        }

        return $this->render('BloggerBlogBundle:Page:contact.html.twig', array(
                    'form' => $form->createView()
        ));
    }

}

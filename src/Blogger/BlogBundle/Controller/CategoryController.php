<?php

namespace Blogger\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CategoryController extends Controller {

    public function ShowAction($category_id) {
//        die($id);
        $em = $this->getDoctrine()->getEntityManager();

//        $blog = $em->getRepository('BloggerBlogBundle:BlogPost')->find($id);


        $blogs = $em->getRepository('BloggerBlogBundle:BlogPost')
                ->getPostByCategory($category_id);


        $categories = $em->getRepository('BloggerBlogBundle:Category')
                ->getAllCategories();
        return $this->render('BloggerBlogBundle:Page:index.html.twig', array(
                    'blogs' => $blogs,'categories'=>$categories
        ));
    }

}

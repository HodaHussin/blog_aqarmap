#** Getting Started** 
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

##**Installing**
1. download the project
1.  open your cmd on windows or terminal on ubuntu
1.  run "composer update" on your cmd to install bundles
1.  run "php bin/console doctrine:schema:create" to create database
1.  run "php bin/console server:run" to run the project
1.  visit "localhost:8000"
1.  To add posts to blog visit "localhost:8000/admin"

# Built With #
1. symfony framework
2. sonat admin bundle
3. bootstrap blog theme